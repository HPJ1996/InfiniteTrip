package com.fcu.hpj_1996.infinitetrip;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private ImageButton start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openCounter();
        View.OnClickListener openMap = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        };

        start = (ImageButton) findViewById(R.id.start);
        start.setOnClickListener(openMap);

    }

    private void openCounter() {

        URL targetUrl = null;
        HttpURLConnection urlConnection = null;
        try {
            targetUrl = new URL("http://www.pink-fun.com.tw/edufor4g/?school=fcu&app=test ");
            urlConnection = (HttpURLConnection) targetUrl.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            //do something
        } catch (Exception e) {
        } finally {
            urlConnection.disconnect();
        }
    }
}

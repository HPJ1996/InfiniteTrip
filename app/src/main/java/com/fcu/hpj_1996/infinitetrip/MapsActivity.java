package com.fcu.hpj_1996.infinitetrip;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {

    private Marker currentMarker;
    private Marker nextPointMarker;
    private GoogleMap mMap;
    private LocationManager lms;
    private LatLng nextPoint;
    TextView mylocation;
    TextView point;
    TextView distance;
    private static int score = 0;
    boolean isFirstPoint = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mylocation = (TextView) findViewById(R.id.gps_location);
        mylocation.setText("現在位置 :");
        point = (TextView) findViewById(R.id.point);
        point.setText("目前分數 : 0分");
        distance = (TextView) findViewById(R.id.distance);
        distance.setText("等待中......請稍後......");

        /*   Location   */
        LocationManager status = (LocationManager) (this.getSystemService(Context.LOCATION_SERVICE));
        if (status.isProviderEnabled(LocationManager.GPS_PROVIDER) || status.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            lms = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location = lms.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            mylocation.setText("現在位置 : " + String.valueOf(location.getLongitude()) + "," + String.valueOf(location.getLatitude()));
            lms.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
            if (mMap != null) {
                setCamera(location);
                setMarker(location);
            }
        } else {
            Toast.makeText(this, "請開啟定位服務", Toast.LENGTH_LONG).show();
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.zoomTo(19)); //放大地圖 18倍
    }

    private void setMarker(Location location) {
        LatLng current = new LatLng(location.getLatitude(), location.getLongitude());
        if(currentMarker == null){

            MarkerOptions mylocation = new MarkerOptions();
            mylocation.icon(BitmapDescriptorFactory.fromResource(R.drawable.pirate));
            mylocation.position(current);
            mylocation.title("I'm here");
            currentMarker = mMap.addMarker(mylocation);
        }else{
            currentMarker.setPosition(current);
            currentMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pirate));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(current));
    }

    private void setCamera(Location location){
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude())));
    }


    @Override
    public void onLocationChanged(Location location) {
        mylocation.setText("現在位置 : " + String.valueOf(location.getLongitude())+","+String.valueOf(location.getLatitude()));
        if(mMap != null)
        {
            setCamera(location);
            setMarker(location);
        }
        if (isFirstPoint) {
            /*LatLng test = new LatLng(location.getLatitude(),location.getLongitude());
            nextPoint = test;*/
            nextPoint = creatNextPoint(location);
            isFirstPoint = false;
        }
        approachPoint(location, nextPoint);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }


    //   Latitude: 1 deg = 110.574 km    Longitude: 1 deg = 111.320*cos(latitude) km

    /*  建立的目的地   */
    public LatLng creatNextPoint(Location loction){

        /*   大約設定在50公尺上下遠   */
        double AddorMinux;
        double rand;
        double latitude;
        double longitude;

        AddorMinux = (int)(Math.random()*10);

        if(AddorMinux > 5) {
            rand = Math.random() / 2000;
            latitude = loction.getLatitude() + rand;
        } else {
            rand = Math.random() / 2000;
            latitude = loction.getLatitude() - rand;
        }

        AddorMinux = (int)(Math.random()*10);

        if(AddorMinux > 5) {
            rand = Math.random() / 2000;
            longitude = loction.getLongitude() + rand;
        } else {
            rand = Math.random() / 2000;
            longitude = loction.getLongitude() - rand;
        }

        LatLng NextPoint = new LatLng(latitude,longitude);

        setNextPointMarker(NextPoint);
        return NextPoint;
    }

    /*   將目的地標記出來   */
    private void setNextPointMarker(LatLng latLng) {
        LatLng nextPoint = new LatLng(latLng.latitude, latLng.longitude);

        if(nextPointMarker == null){
            nextPointMarker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.chest)).position(nextPoint).title("ㄅ ㄒ"));
        }else{
            nextPointMarker.setPosition(nextPoint);
        }
    }

    /*   現在位置與目的位置的距離與判斷   */
    public void approachPoint(Location location,LatLng latLng) {

        double cur_latitude = location.getLatitude();
        double cur_longitude = location.getLongitude();

        double des_latitude = latLng.latitude;
        double des_longitude = latLng.longitude;

        double length;

        length = Math.sqrt((cur_latitude - des_latitude)*(cur_latitude - des_latitude)*110574*110574 + (cur_longitude - des_longitude)*(cur_longitude - des_longitude)*111320*Math.cos(cur_latitude - des_latitude)*11320*Math.cos(cur_latitude - des_latitude));
        distance.setText("距離下一個點 : "+String.valueOf((int)length) + " 公尺");

        /*   設定5公尺內判定抵達目的地   */
        if (length < 5 ) {
            score++;
            point.setText("目前分數 : "+score+" 分");
            setArrivedMarker(latLng,score);
            /*   Dialog   */
            DialogInterface.OnClickListener positive = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            };

            DialogInterface.OnClickListener negative = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            };
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Congratulation !!!");
            alertDialog.setMessage("Do you want to keep traveling ?");
            alertDialog.setPositiveButton("sure",positive);
            alertDialog.setNegativeButton("yes",positive);
            alertDialog.setIcon(R.drawable.chest);
            alertDialog.show();

            /*   建立下個新點   */
            nextPoint = creatNextPoint(location);
        }
    }


    private void setArrivedMarker(LatLng latLng,int score) {
        LatLng nextPoint = new LatLng(latLng.latitude, latLng.longitude);

        mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.cross)).position(nextPoint).title(score+""));
    }

}
